const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const request = require('request-promise-native');

const expressWs = require('express-ws')(app);

let geolocations = [];

// Where you have the static content
app.use(express.static(__dirname + '/public'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// On POST /geolocation
app.post('/geolocation', function(request,response){
  let conversationGeolocation = request.body;
  
  if(conversationGeolocation.conversationId && conversationGeolocation.geolocation){
    // Get details about the geolocation
    getGeolocFromCoordinates(conversationGeolocation.geolocation)
    .then( detailedGeolocation => {
      // Push the geolocation / conversation to the geolocations array 
      geolocations.push({ 
        "conversationId": conversationGeolocation.conversationId,
        "geolocation": detailedGeolocation
      });
      
      console.log({ 
        "conversationId": conversationGeolocation.conversationId,
        "geolocation": detailedGeolocation
      })
      
      let conversationWebsocket = getConversationWebsocket(conversationGeolocation.conversationId);
      if(conversationWebsocket){
        conversationWebsocket.ws.send(JSON.stringify(detailedGeolocation));
      }
      
      response.send(detailedGeolocation);
    })
  }
  else{
    response.sendStatus(400);
  }
});

app.get('/geolocation', function(request,response){
  const conversationId = request.query.conversationId;
  
  // conversationId exists? 
  if(!conversationId){
    response.status(400)
    response.send("Missing conversationId");
  }
  else{
    // Get the first geolocation element from the array for the given conversationId 
    let geolocation = geolocations.find( geoloc => {
      return geoloc.conversationId == conversationId;
    })
    if(geolocation){
      // If a geolocation has been found for this conversationId
      response.send(geolocation.geolocation);
    }else{
      // If no geolocation has been found for this conversationId
      response.status(404);
      response.send("No geolocation found for conversationId " + conversationId);
    }
  }
});


// Get details about the address of a location
const getGeolocFromCoordinates = function(geolocation) {
  return request(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${geolocation.latitude},${geolocation.longitude}&key=AIzaSyBISVHUComgn3W0J6guplfztpsd79FjQTs`)
  .then( dataString => {
    let data = JSON.parse(dataString);
    
    // Look for the address result with type "stret_address" (others results are only POIs, areas, etc.)
    let dataResult = data.results.find( addressResult => {
      return addressResult.types.includes("street_address");
    })
    
    // Map each address component to the geolocation object
    if(dataResult && dataResult.address_components){
      // For each address component
      dataResult.address_components.forEach( comp => {
        // For each type of address component I add a new property to the geolocation object
        //    with key being the type name and the value being the "long_name" of the component
        comp.types.forEach(compType => { geolocation[compType] = comp.long_name });
      })
      
      geolocation.formatted_address = dataResult.formatted_address;
    }
    
    // return the detailed geolocation object
    return geolocation;
  })
}

let websockets = [];
const getConversationWebsocket = function(conversationId){
  return websockets.find(ws => {
    return ws.conversationId == conversationId && ws.ws.readyState == 1; // 1 = OPEN : https://github.com/websockets/ws/blob/master/doc/ws.md#class-websocket
  })
}

app.ws('/', function(ws, req) {
  const conversationId = req.query.conversationId;
  
  websockets.push({
    conversationId: conversationId,
    ws: ws
  })
  
  console.log("Socket " + conversationId + " connected.")
  
  ws.on('message', function(msg) {
    console.log(msg);
  });
});


app.listen(process.env.PORT);
