
const getJsonFromUrl = function() {
  const query = location.search.substr(1);
  let result = {};
  query.split("&").forEach(function(part) {
    const item = part.split("=");
    result[item[0]] = decodeURIComponent(item[1]);
  });
  return result;
}

const conversationId = getJsonFromUrl().conversationId;
const from = getJsonFromUrl().from;

var conversationSocket = new WebSocket(`wss://lmi-geoloc-laurentmillangenesys.c9users.io/?conversationId=${conversationId}`);
conversationSocket.onmessage = function (event) {
  console.log(event.data);
  const data = JSON.parse(event.data);
  console.log(data.accuracy);
  $("#maps").removeClass('d-none');
  $("#not-found").addClass('d-none');
  $("#maps").attr('src', `https://www.google.com/maps/embed/v1/place?q=${data.formatted_address}&key=AIzaSyBOgRi4jGrKABDf9fDZQ5ddvkFV1O37vKM`);
}