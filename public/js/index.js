
const getJsonFromUrl = function() {
  var query = location.search.substr(1);
  var result = {};
  query.split("&").forEach(function(part) {
    var item = part.split("=");
    result[item[0]] = decodeURIComponent(item[1]);
  });
  return result;
}

const conversationId = getJsonFromUrl().convId;

navigator.geolocation.getCurrentPosition(geoloc => {
  // Test
  alert("GeoLoc " + geoloc.coords.latitude + "," + geoloc.coords.longitude + " ("+geoloc.coords.accuracy+")")
  // Envoi de la geoloc en WS

  // Geocoding maps :
  // Response :
  // resp.results[0].formatted_address
  let results = {
    "results": [
      {
        "address_components": [
          {
            "long_name": "37",
            "short_name": "37",
            "types": [
              "street_number"
            ]
          },
          {
            "long_name": "Rue Kléber",
            "short_name": "Rue Kléber",
            "types": [
              "route"
            ]
          },
          {
            "long_name": "Issy-les-Moulineaux",
            "short_name": "Issy-les-Moulineaux",
            "types": [
              "locality",
              "political"
            ]
          },
          {
            "long_name": "Hauts-de-Seine",
            "short_name": "Hauts-de-Seine",
            "types": [
              "administrative_area_level_2",
              "political"
            ]
          },
          {
            "long_name": "Île-de-France",
            "short_name": "Île-de-France",
            "types": [
              "administrative_area_level_1",
              "political"
            ]
          },
          {
            "long_name": "France",
            "short_name": "FR",
            "types": [
              "country",
              "political"
            ]
          },
          {
            "long_name": "92130",
            "short_name": "92130",
            "types": [
              "postal_code"
            ]
          }
        ],
        "formatted_address": "37 Rue Kléber, 92130 Issy-les-Moulineaux, France",
        "geometry": {
          "location": {
            "lat": 48.8246944,
            "lng": 2.2743352
          },
          "location_type": "ROOFTOP",
          "viewport": {
            "northeast": {
              "lat": 48.8260433802915,
              "lng": 2.275684180291502
            },
            "southwest": {
              "lat": 48.8233454197085,
              "lng": 2.272986219708498
            }
          }
        },
        "place_id": "ChIJv6_mfoJ65kcRAtIRRUDR0uA",
        "types": [
          "street_address"
        ]
      },
      {
        "address_components": [
          {
            "long_name": "Mairie d'Issy-Metro",
            "short_name": "Mairie d'Issy-Metro",
            "types": [
              "bus_station",
              "establishment",
              "point_of_interest",
              "transit_station"
            ]
          },
          {
            "long_name": "Issy-les-Moulineaux",
            "short_name": "Issy-les-Moulineaux",
            "types": [
              "locality",
              "political"
            ]
          },
          {
            "long_name": "Hauts-de-Seine",
            "short_name": "Hauts-de-Seine",
            "types": [
              "administrative_area_level_2",
              "political"
            ]
          },
          {
            "long_name": "Île-de-France",
            "short_name": "Île-de-France",
            "types": [
              "administrative_area_level_1",
              "political"
            ]
          },
          {
            "long_name": "France",
            "short_name": "FR",
            "types": [
              "country",
              "political"
            ]
          },
          {
            "long_name": "92130",
            "short_name": "92130",
            "types": [
              "postal_code"
            ]
          }
        ],
        "formatted_address": "Mairie d'Issy-Metro, 92130 Issy-les-Moulineaux, France",
        "geometry": {
          "location": {
            "lat": 48.824772,
            "lng": 2.273991
          },
          "location_type": "GEOMETRIC_CENTER",
          "viewport": {
            "northeast": {
              "lat": 48.8261209802915,
              "lng": 2.275339980291502
            },
            "southwest": {
              "lat": 48.8234230197085,
              "lng": 2.272642019708498
            }
          }
        },
        "place_id": "ChIJmX5_YoJ65kcRh0vPsxQumdY",
        "types": [
          "bus_station",
          "establishment",
          "point_of_interest",
          "transit_station"
        ]
      },
      {
        "address_components": [
          {
            "long_name": "Issy-les-Moulineaux",
            "short_name": "Issy-les-Moulineaux",
            "types": [
              "locality",
              "political"
            ]
          },
          {
            "long_name": "Hauts-de-Seine",
            "short_name": "Hauts-de-Seine",
            "types": [
              "administrative_area_level_2",
              "political"
            ]
          },
          {
            "long_name": "Île-de-France",
            "short_name": "Île-de-France",
            "types": [
              "administrative_area_level_1",
              "political"
            ]
          },
          {
            "long_name": "France",
            "short_name": "FR",
            "types": [
              "country",
              "political"
            ]
          },
          {
            "long_name": "92130",
            "short_name": "92130",
            "types": [
              "postal_code"
            ]
          }
        ],
        "formatted_address": "92130 Issy-les-Moulineaux, France",
        "geometry": {
          "bounds": {
            "northeast": {
              "lat": 48.8346259,
              "lng": 2.28939
            },
            "southwest": {
              "lat": 48.813305,
              "lng": 2.23609
            }
          },
          "location": {
            "lat": 48.8245306,
            "lng": 2.2743419
          },
          "location_type": "APPROXIMATE",
          "viewport": {
            "northeast": {
              "lat": 48.8346259,
              "lng": 2.28939
            },
            "southwest": {
              "lat": 48.813305,
              "lng": 2.23609
            }
          }
        },
        "place_id": "ChIJxy7Ltot65kcRoSduGnfQJos",
        "types": [
          "locality",
          "political"
        ]
      },
      {
        "address_components": [
          {
            "long_name": "92130",
            "short_name": "92130",
            "types": [
              "postal_code"
            ]
          },
          {
            "long_name": "Issy-les-Moulineaux",
            "short_name": "Issy-les-Moulineaux",
            "types": [
              "locality",
              "political"
            ]
          },
          {
            "long_name": "Hauts-de-Seine",
            "short_name": "Hauts-de-Seine",
            "types": [
              "administrative_area_level_2",
              "political"
            ]
          },
          {
            "long_name": "Île-de-France",
            "short_name": "Île-de-France",
            "types": [
              "administrative_area_level_1",
              "political"
            ]
          },
          {
            "long_name": "France",
            "short_name": "FR",
            "types": [
              "country",
              "political"
            ]
          }
        ],
        "formatted_address": "92130 Issy-les-Moulineaux, France",
        "geometry": {
          "bounds": {
            "northeast": {
              "lat": 48.8345737,
              "lng": 2.2893976
            },
            "southwest": {
              "lat": 48.81327049999999,
              "lng": 2.2359652
            }
          },
          "location": {
            "lat": 48.8225067,
            "lng": 2.2687541
          },
          "location_type": "APPROXIMATE",
          "viewport": {
            "northeast": {
              "lat": 48.8345737,
              "lng": 2.2893976
            },
            "southwest": {
              "lat": 48.81327049999999,
              "lng": 2.2359652
            }
          }
        },
        "place_id": "ChIJ4cuRZ4965kcRYIDY4caCCxw",
        "types": [
          "postal_code"
        ]
      },
      {
        "address_components": [
          {
            "long_name": "Hauts-de-Seine",
            "short_name": "Hauts-de-Seine",
            "types": [
              "administrative_area_level_2",
              "political"
            ]
          },
          {
            "long_name": "Île-de-France",
            "short_name": "Île-de-France",
            "types": [
              "administrative_area_level_1",
              "political"
            ]
          },
          {
            "long_name": "France",
            "short_name": "FR",
            "types": [
              "country",
              "political"
            ]
          }
        ],
        "formatted_address": "Hauts-de-Seine, France",
        "geometry": {
          "bounds": {
            "northeast": {
              "lat": 48.9509619,
              "lng": 2.336941
            },
            "southwest": {
              "lat": 48.729351,
              "lng": 2.145702
            }
          },
          "location": {
            "lat": 48.828508,
            "lng": 2.2188068
          },
          "location_type": "APPROXIMATE",
          "viewport": {
            "northeast": {
              "lat": 48.9509619,
              "lng": 2.336941
            },
            "southwest": {
              "lat": 48.729351,
              "lng": 2.145702
            }
          }
        },
        "place_id": "ChIJIX9UJN165kcRQCuLaMOCCwM",
        "types": [
          "administrative_area_level_2",
          "political"
        ]
      },
      {
        "address_components": [
          {
            "long_name": "Île-de-France",
            "short_name": "Île-de-France",
            "types": [
              "administrative_area_level_1",
              "political"
            ]
          },
          {
            "long_name": "France",
            "short_name": "FR",
            "types": [
              "country",
              "political"
            ]
          }
        ],
        "formatted_address": "Île-de-France, France",
        "geometry": {
          "bounds": {
            "northeast": {
              "lat": 49.241504,
              "lng": 3.5590069
            },
            "southwest": {
              "lat": 48.1200811,
              "lng": 1.44617
            }
          },
          "location": {
            "lat": 48.8499198,
            "lng": 2.6370411
          },
          "location_type": "APPROXIMATE",
          "viewport": {
            "northeast": {
              "lat": 49.241504,
              "lng": 3.5590069
            },
            "southwest": {
              "lat": 48.1200811,
              "lng": 1.44617
            }
          }
        },
        "place_id": "ChIJF4ymA8Th5UcRcCWLaMOCCwE",
        "types": [
          "administrative_area_level_1",
          "political"
        ]
      },
      {
        "address_components": [
          {
            "long_name": "Paris Metropolitan Area",
            "short_name": "Paris Metropolitan Area",
            "types": [
              "political"
            ]
          },
          {
            "long_name": "France",
            "short_name": "FR",
            "types": [
              "country",
              "political"
            ]
          }
        ],
        "formatted_address": "Paris Metropolitan Area, France",
        "geometry": {
          "bounds": {
            "northeast": {
              "lat": 49.44956,
              "lng": 3.5590339
            },
            "southwest": {
              "lat": 48.065382,
              "lng": 1.3557489
            }
          },
          "location": {
            "lat": 48.8575712,
            "lng": 2.2771715
          },
          "location_type": "APPROXIMATE",
          "viewport": {
            "northeast": {
              "lat": 49.44956,
              "lng": 3.5590339
            },
            "southwest": {
              "lat": 48.065382,
              "lng": 1.3557489
            }
          }
        },
        "place_id": "ChIJW36m20gL5kcRiMnAEpHgyq8",
        "types": [
          "political"
        ]
      },
      {
        "address_components": [
          {
            "long_name": "France",
            "short_name": "FR",
            "types": [
              "country",
              "political"
            ]
          }
        ],
        "formatted_address": "France",
        "geometry": {
          "bounds": {
            "northeast": {
              "lat": 51.1241999,
              "lng": 9.6624999
            },
            "southwest": {
              "lat": 41.3253001,
              "lng": -5.5591
            }
          },
          "location": {
            "lat": 46.227638,
            "lng": 2.213749
          },
          "location_type": "APPROXIMATE",
          "viewport": {
            "northeast": {
              "lat": 51.1241999,
              "lng": 9.6624999
            },
            "southwest": {
              "lat": 41.3253001,
              "lng": -5.5591
            }
          }
        },
        "place_id": "ChIJMVd4MymgVA0R99lHx5Y__Ws",
        "types": [
          "country",
          "political"
        ]
      }
    ],
    "status": "OK"
  }
});
