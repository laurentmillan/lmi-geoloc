
const getJsonFromUrl = function() {
  const query = location.search.substr(1);
  let result = {};
  query.split("&").forEach(function(part) {
    const item = part.split("=");
    result[item[0]] = decodeURIComponent(item[1]);
  });
  return result;
}

const conversationId = getJsonFromUrl().conversationId;

navigator.geolocation.getCurrentPosition(geoloc => {
  
  const geolocationConversation = {
    "conversationId": conversationId,
    "geolocation": {
      "accuracy": geoloc.coords.accuracy,
      "altitude": geoloc.coords.altitude,
      "altitudeAccuracy": geoloc.coords.altitudeAccuracy,
      "heading": geoloc.coords.heading,
      "latitude": geoloc.coords.latitude,
      "longitude": geoloc.coords.longitude,
      "speed": geoloc.coords.speed
    }
  }

  const posting = $.ajax( {
    "type": "POST",
    "url": location.origin + "/geolocation", 
    "contentType": 'application/json',
    "data": JSON.stringify( geolocationConversation )
  })
  .done(function( data ) {
    console.log("Geoloc postée");
    console.log( data )
    $("#not-found").addClass("d-none");
    $("#found").removeClass("d-none");
    $("#found-text").html(`We have geolocated you at the following address : ${data.formatted_address}.<br><br>This information has been transmitted to your agent.`)
  });
});

if(navigator.userAgent.match(/SamsungBrowser/gi)){
  $("body").css('width', window.screen.width);
}
